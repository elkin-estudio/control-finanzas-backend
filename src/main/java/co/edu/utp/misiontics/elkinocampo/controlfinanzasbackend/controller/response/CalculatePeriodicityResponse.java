package co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.controller.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CalculatePeriodicityResponse implements Serializable {
    private Integer periodicity;
    private String message;
    private String messageInfo;
}
