package co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.controller.request.CalculatePeriodicityRequest;
import co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.controller.response.CalculatePeriodicityResponse;
import co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.service.ControlFinanzasService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("api/controlfinanzas")
public class ControlFinanzasController {

    private final ControlFinanzasService service;

    @PostMapping("/periodicidadPagoprincipal") // POST /api/controlfinanzas/periodicidadPagoprincipal
    public ResponseEntity<CalculatePeriodicityResponse> calculatePrincipalPaymentPeriodicity(
            @RequestBody CalculatePeriodicityRequest request) {

        try {
            var periodicity = service.calculatePeriodicity(request.getDetalle());

            var response = CalculatePeriodicityResponse.builder()
                    .periodicity(periodicity)
                    .build();

            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(CalculatePeriodicityResponse.builder()
                            .message(e.getMessage())
                            .build());
        }
    }
}
