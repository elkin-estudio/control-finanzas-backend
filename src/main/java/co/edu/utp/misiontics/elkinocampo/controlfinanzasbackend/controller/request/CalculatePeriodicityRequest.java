package co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.controller.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CalculatePeriodicityRequest implements Serializable {
    private String detalle;

}
