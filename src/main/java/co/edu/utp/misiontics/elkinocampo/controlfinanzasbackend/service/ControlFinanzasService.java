package co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.service;

public interface ControlFinanzasService {
    
    Integer calculatePeriodicity(String detalle);
    
}
