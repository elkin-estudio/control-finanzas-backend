package co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlFinanzasBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlFinanzasBackendApplication.class, args);
	}

}
