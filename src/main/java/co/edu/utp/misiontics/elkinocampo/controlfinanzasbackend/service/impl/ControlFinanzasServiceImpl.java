package co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.service.impl;

import org.springframework.stereotype.Service;

import co.edu.utp.misiontics.elkinocampo.controlfinanzasbackend.service.ControlFinanzasService;

@Service
public class ControlFinanzasServiceImpl implements ControlFinanzasService {
    @Override

    public Integer calculatePeriodicity(String detalle) {

        var daysPeriodicity = 0;

        switch (detalle) {
            case "Diario":
                daysPeriodicity = 1;
                break;
            case "Semanal":
                daysPeriodicity = 7;
                break;
            case "Quincenal":
                daysPeriodicity = 15;
                break;
            case "Mensual":
                daysPeriodicity = 30;
                break;
            default:
                throw new RuntimeException("La periodicidad no es valida");
        }

        return daysPeriodicity;
    }
}
